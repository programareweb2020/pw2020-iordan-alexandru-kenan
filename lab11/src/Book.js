import React from 'react';

export default ({ book }) => {
    return (
        <tr>
            <td>{book.name}</td>
            <td>{book.author}</td>
            <td>
                {book.genres.map(genre => <span key={genre}>{genre} </span>)}
            </td>
        </tr>
    )
}
