import React from 'react';

export default ({ author, onDelete }) => {
    return (
        <tr>
            <td>{author._id}</td>
            <td>{author.firstName}</td>
            <td>{author.lastName}</td>
            <td><span onClick={onDelete}><i className="fas fa-trash-alt"></i></span></td>
        </tr>
    )
}
