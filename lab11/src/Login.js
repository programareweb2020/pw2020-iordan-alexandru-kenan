import React from 'react';
import { useState } from 'react';
import axios from 'axios'

export default (props) => {
    const [ username, setUsername ] = useState("")
    const [ password, setPassword ] = useState("")

    return (
        <div className="login">
            <form
                style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "flex-start"
                }}
            >
                <label htmlFor="username">Username:</label>
                <input type="test" id="username" value={username} onChange={ev => {setUsername(ev.target.value)}} />

                <label htmlFor="password">Password:</label>
                <input type="password" id="password" value={password} onChange={ev => {setPassword(ev.target.value)}} />

                <div
                    className="login-buttons"
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyItems: "space-between",
                        width: "100%"
                    }}
                >
                    <button
                        style={{display: "block"}}
                        onClick={ev => {
                            axios.post('http://localhost:3001/api/v1/users/register', {
                                username,
                                password
                            }).then(response => {console.log(response.data)})
                                .catch(error => console.log(error))
                        }}
                    >
                        Register
                    </button>

                    <button
                        style={{display: "block"}}
                        onClick={ev => {
                            axios.post('http://localhost:3001/api/v1/users/login', {
                                username,
                                password
                            })
                                .then(response => {
                                    localStorage.setItem('token', response.data)
                                })
                                .catch(error => console.log(error))
                        }}
                    >
                        Log In
                    </button>
                </div>
            </form>
        </div>
    )
}
