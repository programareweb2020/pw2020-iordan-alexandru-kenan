import React from 'react';
import { useState, useEffect } from 'react';
import axios from 'axios'
import Book from './Book'

export default (props) => {
    const [ books, setBooks ] = useState(null)
    const [ newAuthorId, setNewAuthorId ] = useState("")
    const [ newName, setNewName ] = useState("")
    const [ newGenres, setNewGenres ] = useState("")

    useEffect(() => {
        axios.get('http://localhost:3001/api/v1/books', {
            headers: {
                Authorization: `Bearer: ${localStorage.getItem('token')}`
            }
        })
            .then(response => setBooks(response.data))
            .catch(error => console.log(error))
    }, [])

    return (
        <div>
            <div>
            {
                books === null
                    ?
                        <p>Loading...</p>
                        :
                        <table>
                            <thead>
                                <tr>
                                    <td>Title</td>
                                    <td>Author</td>
                                    <td>Genres</td>
                                </tr>
                            </thead>

                            <tbody>
                                {books.map(book => <Book book={book} key={book.id}/>)}
                            </tbody>
                        </table>
            }
            </div>

            <form onSubmit={ev => {
                    ev.preventDefault()

                    axios.post('http://localhost:3001/api/v1/books', {
                        authorId: newAuthorId,
                        name: newName,
                        genres: newGenres.split(' ')
                    },
                    {
                        headers: {
                            Authorization: `Bearer: ${localStorage.getItem('token')}`
                        },
                    })
                        .then(response => {
                            axios.get('http://localhost:3001/api/v1/books', {
                                headers: {
                                    Authorization: `Bearer: ${localStorage.getItem('token')}`
                                }
                            })
                                .then(response => setBooks(response.data))
                                .catch(error => console.log(error))
                        })
                        .catch(error => {
                            if (error.response) {
                                console.log(error.response)
                                alert(error.response.data.error)
                            }
                        })
                }}
            >
                <fieldset>
                    <legend>New Book</legend>

                    <label>
                        Author ID:
                        <input type="text" id="authorId" value={newAuthorId} onChange={ev => {
                            setNewAuthorId(ev.target.value)
                        }}/>
                    </label>

                    <label>
                        Name:
                        <input type="text" id="name" value={newName} onChange={ev => {
                            setNewName(ev.target.value)
                        }}/>
                    </label>

                    <label>
                        Genres:
                        <input type="text" id="genres" value={newGenres} onChange={ev => {
                            setNewGenres(ev.target.value)
                        }}/>
                    </label>

                    <input type="submit" value="Add Book" />
                </fieldset>
            </form>
        </div>
    )
}
