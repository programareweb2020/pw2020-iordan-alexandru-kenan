import React from 'react';
import { useState, useEffect } from 'react';
import axios from 'axios'
import Author from './Author'

export default (props) => {
    const [ authors, setAuthors ] = useState(null)
    const [ newFirstName, setNewFirstName ] = useState("")
    const [ newLastName, setNewLastName ] = useState("")

    useEffect(() => {
        axios.get('http://localhost:3001/api/v1/authors', {
            headers: {
                Authorization: `Bearer: ${localStorage.getItem('token')}`
            }
        })
            .then(response => setAuthors(response.data))
            .catch(error => console.log(error))
    }, [])

    const deleteAuthor = id => {
        axios.delete(`http://localhost:3001/api/v1/authors/${id}`, {
            headers: {
                Authorization: `Bearer: ${localStorage.getItem('token')}`
            }
        })

        setAuthors(authors.filter(a => a._id !== id))
    }

    return (
        <div>
            <div>
            {
                authors === null
                    ?
                        <p>Loading...</p>
                        :
                        <table>
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                {authors.map(author => <Author author={author} key={author._id} onDelete={() => deleteAuthor(author._id)}/>)}
                            </tbody>
                        </table>
            }
            </div>

            <form
                onSubmit={ev => {
                    ev.preventDefault()

                    axios.post('http://localhost:3001/api/v1/authors', {
                        firstName: newFirstName,
                        lastName: newLastName
                    },
                    {
                        headers: {
                            Authorization: `Bearer: ${localStorage.getItem('token')}`
                        },
                    })
                        .then(response => {
                            axios.get('http://localhost:3001/api/v1/authors', {
                                headers: {
                                    Authorization: `Bearer: ${localStorage.getItem('token')}`
                                }
                            })
                                .then(response => setAuthors(response.data))
                                .catch(error => console.log(error))
                        })
                        .catch(error => {
                            if (error.response) {
                                console.log(error.response)
                                alert(error.response.data.error)
                            }
                        })
                }}
            >
                <fieldset>
                    <legend>New Author</legend>
                    <label>
                        First Name:
                        <input
                            type="text"
                            id="firstName"
                            value={newFirstName}
                            onChange={ev => {
                                setNewFirstName(ev.target.value)
                            }}
                        />
                    </label>

                    <label>
                        Last Name:
                        <input
                            type="text"
                            id="lastName"
                            value={newLastName}
                            onChange={ev => {
                                setNewLastName(ev.target.value)
                            }}
                        />
                    </label>

                    <input
                        type="submit"
                        values="Add Author"
                    />
                </fieldset>
            </form>
        </div>
    )
}
