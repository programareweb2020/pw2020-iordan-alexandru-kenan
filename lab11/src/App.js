import React from 'react';
import { HashRouter, Switch, Route, Link } from 'react-router-dom';
import Login from './Login'
import BooksList from './BooksList'
import AuthorsList from './AuthorsList'

function App() {
    return (
        <HashRouter basename='/'>
            <Switch>
                <Route exact path={'/'} component={() =>
                <ul>
                    <li>Login <Link to='/login'>here</Link></li>
                    <li>Books <Link to='/books'>here</Link></li>
                    <li>Authors <Link to='/authors'>here</Link></li>
                </ul>
                } />
                <Route exact path={'/login'} component={Login} />
                <Route exact path={'/books'} component={BooksList} />
                <Route exact path={'/authors'} component={AuthorsList} />
            </Switch>
        </HashRouter>
    );
}

export default App;
