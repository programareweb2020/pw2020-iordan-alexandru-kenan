const express = require('express');
const app = express();
app.use(express.json());
app.get('/hello', (req, res) => { // -> creaza o ruta GET cu url-ul '/hello'
  res.send('Hello!');
});

app.post('/marco', (req, res) => { // -> creaza o ruta POST cu url-ul '/marco'
  if (req.body.replyBack === true) {
    res.send('Polo!');
  } else {
    res.send('...');
 }
});

app.delete('/order', (req, res) => { // -> creaza o ruta DELETE cu url-ul '/order'
  res.send('Deleted order');
});

app.put('/user', (req, res) => { // -> creaza o ruta PUT cu url-ul '/user'
  res.send('Changed user');
});

const port = 3000;
app.listen(port, () => console.log(`Salut, rulez pe portul ${port}!`));
