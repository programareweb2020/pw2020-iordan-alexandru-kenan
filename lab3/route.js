const express = require('express');
const database = require('./database.js');

const router = express.Router();

//Insert book into database
router.post('/', (req, res) => {
	const book = req.body;

	database.insertIntoDb(book);
	res.status(201).json(book);
});

//Get all from db
router.get('/all', (req, res) => {
	const books = database.getAllFromDb();
	res.status(200).json(books);
	});


//Get by id
router.get('/:id', (req, res) => {
	const book = database.getFromDbById(parseInt(req.params.id));
	res.status(200).json(book);
	});

//Get by author
router.get('/', (req, res) => {
    if (req.query.author) {
        const books = database.getFromDbByAuthor(req.query.author);
        res.status(200).json(books);
    } else {
        const books = database.getAllFromDb();
        res.status(200).json(books);
    }
});


//Delete by id
router.delete('/:id', (req, res) => {
	const books = database.removeFromDbById(parseInt(req.params.id));
	res.status(200).json("Deleted id");
	});

//Delete by author
router.delete('/', (req, res) => {
    if (req.query.author) {
        const books = database.removeFromDbByAuthor(req.query.author);
        res.status(200).json("Deleted author");
    } else {
        const books = database.purgeDb();
        res.status(200).json("Database deleted");
    }
});

//Update by id


//Purge database
router.delete('/', (req, res) => {
	const books = database.purgeDb();
	res.status(200).json("Database deleted");
	});

module.exports = router;
