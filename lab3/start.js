const express = require('express');
const router = require('./route.js');

const app = express();

app.use(express.json());

app.use('/book', router); // -> observati cum /menus este scris o singura data, in calea data functiei use

app.listen(3000, () => console.log('App is listening on port 3000'));
