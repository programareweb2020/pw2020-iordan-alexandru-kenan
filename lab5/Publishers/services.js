const {
    query
} = require('../data');

const add = async (name) => {
    await query('INSERT INTO publishers (name) VALUES ($1)', [name]);
};

const getAll = async () => {
    return await query('SELECT * FROM publishers');
};

const getById = async (id) => {
    return await query('SELECT name FROM publishers WHERE id = $1', [id]);
};

const updateById = async (id, name) => {
    console.log(name);
    await query('UPDATE publishers SET name = $1 WHERE id = $2', [name, id]);
};

const deleteById = async (id) => {
    await query('DELETE FROM publishers WHERE id = $1', [id]);
};

const getBooksById = async (id) => {
    return await query(
        'SELECT books.name \
        FROM books \
        JOIN publishers_books ON books.id = publishers_books.book_id \
        JOIN publishers ON publishers.id = publishers_books.publisher_id \
        WHERE publishers.id = $1', [id]
    );
};

module.exports = {
    add,
    getAll,
    getById,
    updateById,
    deleteById,
    getBooksById
}