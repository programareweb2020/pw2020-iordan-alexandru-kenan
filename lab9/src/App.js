import React, {useEffect, useState} from 'react';
import logo from './lion.svg';
import styles from './App.module.scss';

function Counter(props) {
    const [count, setCount] = useState(props.initialValue);

    const onClickIncrement = () => setCount(props.increment);
    const onClickDecrement = () => setCount(props.decrement);
    const onClickReset = () => setCount(props.reset);

    useEffect( () => {
      if (count === 0) {
        alert("Counter is 0");
      }
    });

    return (
      <div className={styles.counter}>
        <h2>{count}</h2>
        <button onClick={onClickIncrement}>Increment</button>
        <button onClick={onClickDecrement}>Decrement</button>
        <button onClick={onClickReset}>Reset</button>
      </div>
    );
}

function Header(props) {
  return <h1 className={styles.header}>Header</h1>;
}

function Nav(props) {
  return <nav className={styles.nav}>
    <div className={styles.navLeft}>
      <img src={logo}/>
        <a className={styles.navmenu}>Home</a>
        <a className={styles.navmenu}>Books</a>
        <a className={styles.navmenu}>Authors</a>
    </div>
    <div className={styles.navRight}>
      <select className={styles.dropdown}>
        <option>Logout</option>
      </select>
    </div>
  </nav>;
}

function Footer(props) {
  return <footer className={styles.footer}>
  Iordan Alexandru-Kenan
  <br/>
  Facultatea de Automatica si Calculatoare, UPB
  <br/>
  An absolvire: 2020
  </footer>;
}

function Layout(props) {
  return <div className={styles.layout}>{props.children}</div>
}


function App() {
  return (
    <div>
      <Layout>
        <Counter initialValue={0} increment={x => x + 1} decrement={x => x - 1} reset={x => 0}></Counter>
        <Header></Header>
        <Nav></Nav>
        <Footer></Footer>
      </Layout>
    </div>
  );
}


export default App;
