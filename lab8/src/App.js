import React from 'react';
import logo from './logo.svg';
import './App.css';

function Counter() {
        const [state, setState] = React.useState(0);

        const onClickIncrement = () => setState(state + 1);
        const onClickDecrement = () => setState(state - 1);
        const onClickReset = () => setState(0);


        return (
            <div>
                <h1 align="center">{state}</h1>
                <button onClick={onClickIncrement}> Increment </button>
                <button onClick={onClickDecrement}> Decrement </button>
                <button onClick={onClickReset}> Reset </button>
            </div>
        );
}

function Header(props) {
    return <h1 className='Header'> {props.name} </h1>;
}

function Nav(props) {
    return <nav className='Nav'> {props.name} </nav>;
}

function Footer(props) {
    return <footer className='Footer'> {props.name} </footer>;
}

function Layout(props) {
    return <div className='Layout'> {props.children} </div>
}

function App() {
  return (
        <div>
            <Layout>
                <Header name='My Header'></Header>
                <Nav name='My Nav'></Nav>
                <Counter></Counter>
                <Footer name='My Footer'></Footer>
            </Layout>
        </div>
    );

}

export default App;
