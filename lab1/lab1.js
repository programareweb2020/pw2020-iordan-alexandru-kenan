/////Laborator 1

// task 2
console.log("Hello World!");

// task 3
console.log(new Date().toLocaleString());

// task 4
var arr = [];
for(var i = 0; i < 101; ++i) {
  arr.push(i);
  if(i % 2 === 0)
    console.log(i);
}

// task 5
var func = (arr, index) => arr[index];
function task5(arr, index, func) {
  return func(arr, index);
}
task5(arr, 3, func);
