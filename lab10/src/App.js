import React, {useEffect, useState} from 'react';
import { HashRouter, Switch, Route, Link, useRouteMatch } from 'react-router-dom';
import logo from './logo.svg';
import './App.module.scss';

const axios = require('axios');

function NavBar() {
  return (
    <div>
      <button>
        <Link to="/login">Login</Link>
      </button>
    </div>
  );
}

function Main() {
  let match = useRouteMatch();
  return(
    <div>
      <button>
        <Link to={`${match.url}/authors`}>Authors</Link>
      </button>
      <button>
        <Link to={`${match.url}/books`}>Books</Link>
      </button>

      <Switch>
        <Route path={`${match.url}/books`}>
            <Books />
        </Route>
        <Route path={`${match.url}/authors`}>
            <Authors />
        </Route>
      </Switch>
    </div>
  );
}

function Login() {
  function check_login() {
    axios.post("http://localhost:3001/api/v1/users/login", {
      username: document.getElementById('username').value,
      password: document.getElementById('password').value
    }).then(response => localStorage.setItem("token", response.data.token)).catch(error => console.error(error));
  }

  return (
    <div>
      <label for="username">Username: </label>
      <input type="text" id="username"/><br/>
      <label for="password">Password: </label>
      <input type="text" id="password"/><br/>
      <button onClick={check_login}>
        <Link to="/main">Login</Link>
      </button>
      <label>
        <Link to="/register"> Register now </Link>
      </label>
    </div>
  );
}

function Register() {
  function check_register() {
    axios.post("http://localhost:3001/api/v1/users/register", {
      username: document.getElementById('username').value,
      password: document.getElementById('password').value
    }).then(response => console.log("Am facut un nou user: " + response)).catch(error => console.error(error));
  }

  return (
    <div>
      <label for="username">Username: </label>
      <input type="text" id="username"/><br/>
      <label for="password">Password: </label>
      <input type="text" id="password"/><br/>
      <button onClick={check_register}>
        <Link to="/login">Submit</Link>
      </button>
      <button>
        <Link to="/login">Back</Link>
      </button>
    </div>
  );
}

function Books() {
  let books;
  axios.get("localhost:3001/api/v1/books", {
      headers: {
        Authorization: 'Bearer $(localStorage.getItem(token))'
      }
    }).then(response => books = response.data).catch(error => console.error(error));

  return (
    <div>
      <p>{books}</p>
    </div>
  );
}

function Authors() {
  let authors;
  axios.get("localhost:3001/api/v1/authors", {
      headers: {
        Authorization: 'Bearer $(localStorage.getItem(token))'
      }
    }).then(response => authors = response.data).catch(error => console.error(error));
  console.log(localStorage);
  return (
    <div>
      <p>{authors}</p>
    </div>
  );
}

function App() {
  return (
    <HashRouter>
      <Switch>
        <Route exact path="/">
            <NavBar />
        </Route>
        <Route path="/login">
            <Login />
        </Route>
        <Route path="/register">
            <Register />
        </Route>
        <Route path="/main">
            <Main />
        </Route>
      </Switch>
    </HashRouter>
  );
}

export default App;
