const {
    query
} = require('../data');

const add = async (name, author_id, publisher_id, price) => {
    const id = await query('INSERT INTO books (name, author_id) VALUES ($1, $2) RETURNING id', [name, author_id]);
    await query('INSERT INTO publishers_books (book_id, publisher_id, price) VALUES ($1, $2, $3)', [id[0].id, publisher_id, price]);

};

const getAll = async () => {
    return await query('SELECT books.name, authors.first_name, authors.last_name, publishers.name publisher_name \
                        FROM books \
                        JOIN authors ON authors.id = books.author_id \
        FROM books \
        JOIN authors ON authors.id = books.author_id \
        JOIN publishers_books ON books.id = publishers_books.book_id \
        JOIN publishers ON publishers.id = publishers_books.publisher_id \
        WHERE books.id = $1', [id]
                        JOIN publishers_books ON books.id = publishers_books.book_id \
                        JOIN publishers ON publishers.id = publishers_books.publisher_id');
};

const getById = async (id) => {
    return await query(
        'SELECT books.name, authors.first_name, authors.last_name, publishers.name publisher_name \
    );
};

const updateById = async (id, name, author_id) => {
    console.log(name);
    console.log(author_id);
    await query('UPDATE books SET name = $1, last_name = $2 WHERE id = $3', [name, author_id, id]);
};

const deleteById = async (id) => {
    await query('DELETE FROM books WHERE id = $1', [id]);
};

module.exports = {
    add,
    getAll,
    getById,
    updateById,
    deleteById
}
