import React from "react";
import Show from "./show"

export default class Result extends React.Component {

    constructor(props) {

        super(props);
        this.state = {
            isLoaded: false,
            albums: [],
        };
    }

    componentDidMount() {
        this.getAlbums();
    }

    getAlbums() {

        fetch('https://jsonplaceholder.typicode.com/albums')
            .then(response => response.json())
            .then(json => {
                this.setState({ albums: json });
                this.getPhotos();
            })
    }

    getPhotos() {

        this.state.albums.map(e => {

            let url = 'https://jsonplaceholder.typicode.com/album/' + e.id + '/photos'
            fetch(url)
                .then(response => response.json())
                .then(json => {
                    e.photo = json[0].thumbnailUrl
                    this.setState({e: json[0].thumbnailUrl})
                })
        })
        this.getUsers()
    }

    getUsers() {

        this.state.albums.map(e => {

            let url = 'https://jsonplaceholder.typicode.com/album/' + e.id + '/users'
            fetch(url)
                .then(response => response.json())
                .then(json => {
                    e.name = json[0].name
                    e.username = json[0].username
                    this.setState({e: json[0].name})
                })
        })
        this.setFinish()
    }

    setFinish() {
        this.setState({isLoaded : true})
    }

    render() {

        const { isLoaded, albums } = this.state;

        let show;

        if(isLoaded == true) {
            show = <Show data={albums}/>
        }
        else {
            show = <h1>Not loaded yet</h1>
        }

        return (
            <div>
            {show}
            </div>
        );
    }

}
