import React from 'react';
import './App.css';
import Result from './result'
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {

  return (
    <div className="App">
      <Result/>
    </div>
  );
}

export default App;
