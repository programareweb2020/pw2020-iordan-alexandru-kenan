import React from "react";
import Image from 'react-bootstrap/Image'
import Card from 'react-bootstrap/Card'
import Pagination from 'react-bootstrap/Pagination'
import Carousel from 'react-bootstrap/Carousel'

export default class Show extends React.Component {

    constructor(props) {

        super(props);

        let nr_items = (props.data.length) / 10
        let arr = []
        for (let i = 1; i <= nr_items; i++)
            arr.push(i)

        this.state = {
            albums: props.data,
            items: [],
            nr_items: arr,
            photos: []
        };
        this.pageChanged = this.pageChanged.bind(this);
    }

    pageChanged(e) {

        let page_nr = e.target.text
        let items = [];
        for (let number = 1 * page_nr; number <= 10 * page_nr; number++) {

            items.push(
                <Card key={number} className="text-center" >
                    <Card.Body>
                        <Card.Title className="test">{this.state.albums[number].title}</Card.Title>
                        <Card.Text className="test">
                            by {this.state.albums[number].username} aka {this.state.albums[number].name}
                        </Card.Text>
                        <Image src={this.state.albums[number].photo} />
                        <button onClick={() => this.getAllPhotos(number)}>Get Album's photos</button>
                    </Card.Body>
                </Card>
            );
        }
        this.setState({ items: items })
    }

    getAllPhotos(number) {

        let arr = []
        let url = 'https://jsonplaceholder.typicode.com/album/' + number + '/photos'
        fetch(url)
            .then(response => response.json())
            .then(json => {
                for (let i of json)
                    arr.push(i.url)
                this.setState({ photos: arr })
            })
    }

    render() {

        return (
            <div className="App">
                <h1>Lab12</h1>

                <br />

                <Pagination onClick={this.pageChanged}>
                    {this.state.nr_items.map(e => (
                        <Pagination.Item key={e}>
                            {e}
                            <br />
                            {this.state.items.map((el, index) => (
                                <div key={e * index}>
                                    {el}
                                </div>
                            ))}
                        </Pagination.Item>
                    ))}
                </Pagination>

                <Carousel>
                    {this.state.photos.map((p, index) => (

                        <Carousel.Item key={index}>
                            <img  className="App" src={p} alt="test" />
                            <Carousel.Caption>
                                <p>Caption</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                    ))}
                </Carousel>

            </div >
        )
    }
}
