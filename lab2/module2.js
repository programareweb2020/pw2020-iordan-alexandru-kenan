const module1 = require('./module1.js');
const sum = (vec, parNum) => {
  vec = vec.filter(e => e % 2 === parNum % 2);
  return module1.sum(vec);
};
module.exports = {
  sum
};
