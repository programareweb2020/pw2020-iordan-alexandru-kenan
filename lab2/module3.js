const module2 = require('./module2.js');
let vec = [1,2,3,4,5,6,7,8],
  parNum = 0;
var ret = module2.sum(vec, parNum);

const express = require('express');

const app = express();

app.get('/', (req, res) => {
    res.send({sum: ret});
});

app.listen(3000);
